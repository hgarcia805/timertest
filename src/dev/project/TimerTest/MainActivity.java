package dev.project.TimerTest;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	int _timeDefaultTotalSeconds = 60, _timeRemainingTotalSeconds;
	TextView _textViewMinutes, _textViewSeconds;
	CountDownTimer _timer;
	Button _buttonStartStop, _buttonReset;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		_timeRemainingTotalSeconds = _timeDefaultTotalSeconds;

		_textViewMinutes = (TextView) findViewById(R.id.textViewMinutes);
		_textViewSeconds = (TextView) findViewById(R.id.textViewSeconds);

		updateDisplay();

		_buttonStartStop = (Button) findViewById(R.id.buttonStart);
		_buttonStartStop.setOnClickListener(timerStart_OnClick);

		_buttonReset = (Button) findViewById(R.id.buttonReset);
		_buttonReset.setOnClickListener(timerReset_OnClick);
	}

	public void updateDisplay() {
		_textViewMinutes.setText(String.format("%02d", _timeRemainingTotalSeconds / 60));
		_textViewSeconds.setText(String.format("%02d",_timeRemainingTotalSeconds % 60));
	}

	protected final void timerStart() {
		;
		_buttonStartStop.setText(R.string.pause);
		_timer = new CountDownTimer((_timeRemainingTotalSeconds * 1000), 500) {

			public void onTick(long millisUntilFinished) {
				_timeRemainingTotalSeconds = (int) millisUntilFinished / 1000;
				updateDisplay();
			}

			public void onFinish() {
				timerReset();
			}

		};

		_timer.start();
	}

	void timerPause() {
		_timer.cancel();
		_buttonStartStop.setText(R.string.resume);
		Log.i("pauseTimer", "timer paused");
	}

	void timerReset() {
		timerPause();
		_timeRemainingTotalSeconds = _timeDefaultTotalSeconds;
		_buttonStartStop.setText(R.string.start);
		updateDisplay();

	}

	View.OnClickListener timerStart_OnClick = new OnClickListener() {
		public void onClick(View v) {
			if (isStopped()) {
				timerStart();
				Log.i("startTimer", "timer started");
			} else {
				timerPause();
				Log.i("timerPause", "timer paused");
			}
		}
	};
	
	boolean isStopped()
	{
		if (_buttonStartStop.getText() == getResources().getString(R.string.start)) 
			return true;
		if (_buttonStartStop.getText() == getResources().getString(R.string.resume)) 
			return true;
		return false;
	}

	View.OnClickListener timerReset_OnClick = new OnClickListener() {
		public void onClick(View v) {
			timerReset();
			Log.i("timerReset", "timer reset");
		}
	};

}
